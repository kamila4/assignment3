package AbstractDemo;

public abstract class Bank {
	double balance;
	public void setBalance(double balance) {
		this.balance = balance;
	}
	abstract double getBalance();
}
